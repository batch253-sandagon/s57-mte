let collection = [];

// Write the queue functions below.

//  Print queue elements.
function print() {
    return collection;
};

//  Enqueue a new element.
function enqueue(newElement) {
    collection.push(newElement);
    return collection;
};

// Dequeue the first element.
function dequeue() {
    collection.shift();
    return collection;
};

// Get first element.
function front() {
    return collection[0];
};

//  Get queue size.
function size() {
    return collection.length;
};

//  Check if queue is not empty.
function isEmpty() {
    return collection.length === 0 ? true : false;
}

module.exports = {
    print, enqueue, dequeue, front, size, isEmpty
};